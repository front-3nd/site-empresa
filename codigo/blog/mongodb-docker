<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>MongoDb Docker | balta.io</title>

<meta name="description" content="Neste artigo vamos aprender a utilizar o MongoDb via Docker, dispensando a necessidade de um servi&#xE7;o sempre em execu&#xE7;&#xE3;o na sua m&#xE1;quina de desenvolvimento.">
<meta name="keywords" content="mongo db mongodb docker">
<meta name="author" content="balta.io">

<!--Twitter Share-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="https://balta.io/artigos/mongodb-docker" />
<meta name="twitter:creator" content="@balta_io" />
<meta name="twitter:title" content="MongoDb Docker" />
<meta name="twitter:description" content="Neste artigo vamos aprender a utilizar o MongoDb via Docker, dispensando a necessidade de um servi&#xE7;o sempre em execu&#xE7;&#xE3;o na sua m&#xE1;quina de desenvolvimento." />
<meta name="twitter:image" content="https://baltaio.blob.core.windows.net/static/images/articles/mongodb-docker_share.jpg" />

<!--Facebook/LinkedIn Share-->
<meta property="og:url" content="https://balta.io/artigos/mongodb-docker" />
<meta property="og:title" content="MongoDb Docker - balta.io" />
<meta property="og:description" content='Neste artigo vamos aprender a utilizar o MongoDb via Docker, dispensando a necessidade de um servi&#xE7;o sempre em execu&#xE7;&#xE3;o na sua m&#xE1;quina de desenvolvimento.' />
<meta property="og:image" content='https://baltaio.blob.core.windows.net/static/images/articles/mongodb-docker_share.jpg' />

<link rel="icon" type="image/x-icon"
      href="https://baltaio.blob.core.windows.net/static/images/icons/favicon.ico?v1" />
<link rel="shortcut icon" type="image/x-icon"
      href="https://baltaio.blob.core.windows.net/static/images/icons/favicon.ico?v1" />
<link rel="stylesheet" href="https://baltaio.blob.core.windows.net/static/css/uikit.balta.dark.css?v1" />

<link rel="stylesheet" href="/css/site.css?v=Xf9wDwoEu67xOdNRvXvQdyfFF6GZqkqgMYikpsnQG3o">
    
    <meta property="article:author" content="Andr&#xE9; Baltieri">
    <meta property="article:section" content="Backend">
    <meta property="article:tag" content="mongo db mongodb docker">
    <meta property="article:published_time" content="23/08/2020 00:00:00">

</head>

<body>

    <div class="uk-alert-primary announcement" uk-alert>
        <button class="primary uk-alert-close" type="button" uk-close></button>
        <p class="uk-text-center">
            <a href="https://balta.io/cursos/aplicacoes-mult-tenant-entity-framework-core" class="no-link" target="_blank">
                <span uk-icon="heart"></span> CURSO NOVO: MULT TENANT COM EF CORE
            </a>
        </p>
    </div>


<header>
    <div class="uk-container">
        <nav class="uk-navbar-container uk-margin-top uk-margin-large-bottom" uk-navbar>
    <div class="uk-navbar-left">
        <button class="uk-icon-button uk-hidden@m" uk-icon="menu" uk-toggle="target: #side-menu"></button>
        <a href="/">
            <img class="uk-visible@m"
                 src="https://baltaio.blob.core.windows.net/static/images/dark/balta-logo.svg" width="180"
                 alt="balta.io" />
            <img class="uk-hidden@m"
                 src="https://baltaio.blob.core.windows.net/static/images/dark/balta-logo.svg" width="150"
                 alt="balta.io" />
        </a>
        <ul class="uk-navbar-nav uk-margin-small-left uk-visible@s">
            <li>
                <a href="/cursos">Cursos</a>
            </li>
        </ul>
    </div>
    <div class="uk-navbar-right">

        <ul class="uk-navbar-nav uk-visible@m">
            <li>
                <a href="/carreiras">Carreiras</a>
            </li>
            <li>
                <a href="/como-funciona">Como funciona?</a>
            </li>
            <li>
                <a href="/agenda">Agenda</a>
            </li>
            <li>
                <a href="/blog">Blog</a>
            </li>
            <li>
                <a target="_blank" href="/blog/rss">
                    <span uk-icon="icon: rss; ratio: 0.8"></span>
                </a>
            </li>
        </ul>

        <hr class="uk-divider-vertical uk-visible@m">

        <a title="Ir para o Player" class="uk-icon-button uk-button-default uk-margin-right" uk-icon="tv" href="/player">
        </a>

            <a class="uk-button uk-button-secondary uk-margin-left uk-visible@s" href="/premium">
                <span class="uk-margin-small-right uk-visible@l" uk-icon="icon: star; ratio: 0.6"></span>
                <span class="uk-visible@l">Seja</span> Premium
            </a>

    </div>
</nav>


<div id="side-menu" uk-offcanvas>
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>

        <div class="uk-flex uk-flex-between uk-flex-column uk-height-1-1">
            <div>
                <img data-src="https://baltaio.blob.core.windows.net/static/images/dark/balta-logo.svg" uk-img
                     alt="balta.io" />
                <br /><br />
                <ul class="uk-nav">
                    <li>
                        <a href="/cursos">Cursos</a>
                    </li>
                    <li>
                        <a href="/carreiras">Carreiras</a>
                    </li>
                    <li>
                        <a href="/como-funciona">Como funciona?</a>
                    </li>
                    <li>
                        <a href="/agenda">Agenda</a>
                    </li>
                    <li>
                        <a href="/blog">Blog</a>
                    </li>
                    <li>
                        <a href="/player">
                            <div class="uk-flex uk-flex-between uk-flex-middle">
                                Player
                                <span uk-icon="icon: tv; ratio: 1.2"></span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>

            <div>
                    <a class="uk-button uk-button-secondary uk-width-1-1" href="">
                        <span class="uk-margin-small-right" uk-icon="icon: star; ratio: 0.6"></span>
                        Seja Premium
                    </a>

                    <p class="uk-text-center">
                        <a href="/entrar"><strong>Entre</strong> ou <strong>Cadastre-se</strong></a>
                    </p>
            </div>
        </div>
    </div>
</div>
    </div>
</header>

<main>
    
<div class="uk-container">
    <ul class="uk-breadcrumb">
        <li><a href="/">Home</a></li>
        <li><a href="/blog">Artigos</a></li>
        <li><span>MongoDb Docker</span></li>
    </ul>
</div>

<br>
<br>

<div class="uk-container">
    <div class="uk-flex uk-flex-between" uk-grid>
        <div class="uk-width-1-2@s uk-margin-large-top uk-margin-large-bottom">
            <article>
                <h1>MongoDb Docker</h1>

                <p>Neste artigo vamos aprender a utilizar o MongoDb via Docker, dispensando a necessidade de um serviço sempre em execução na sua máquina de desenvolvimento.</p>
<h2 id="requisitos">Requisitos</h2>
<h3 id="terminal">Terminal</h3>
<p>Se estiver no Mac ou Linux, provavelmente já tem um Terminal bacana, mas caso esteja utilizando o Windows, sugiro fortemente que utilize o <strong>Windows Terminal</strong> para estas ações.</p>
<p>Aqui tem um link com a instalação e configurações que uso:
<a href="https://balta.io/blog/windows-terminal">https://balta.io/blog/windows-terminal</a></p>
<h2 id="docker">Docker</h2>
<p>Você precisa também do <strong>Docker</strong> instalado e rodando em sua máquina. Caso ainda não tenha realizado esta instalação, aqui está um artigo que mostro como fazer: <a href="https://balta.io/blog/docker-instalacao-configuracao-e-primeiros-passos">https://balta.io/blog/docker-instalacao-configuracao-e-primeiros-passos</a></p>
<h3 id="wsl-2-somente-windows">WSL 2 (Somente Windows)</h3>
<p>Caso esteja no Windows, recomendo fortemente que instale e utilize o <strong>WSL</strong> que é um subsistema Linux dentro do Windows.</p>
<p>Este processo tem que ser feito <strong>antes</strong> da instalação do Docker.
Basta acessar este link e realizar a instalação: <a href="https://balta.io/blog/wsl">https://balta.io/blog/wsl</a></p>
<h2 id="obtendo-a-imagem">Obtendo a imagem</h2>
<p>Certifique-se que o Docker está em execução e abra um novo terminal, no meu caso, a versão do Docker em execução é a mostrada abaixo. Você pode verificar isto executando o comando <code>docker --version</code> no seu terminal.</p>
<pre><code>Docker version 19.03.12, build 48a66213fe
</code></pre>
<p>Nosso primeiro passo então é obter a imagem do Mongo que será o molde para criarmos nossos containers. Para isto, executamos o comando abaixo.</p>
<pre><code>docker pull mongo
</code></pre>
<p>Note que a primeira mensagem será <code>Using default tag: latest</code> o que significa que estamos obtendo a última versão desta imagem, provavelmente com a última versão estável do Mongo.</p>
<p>Caso queira baixar alguma versão específica, verifique as <a href="https://hub.docker.com/_/mongo?tab=tags">tags disponíveis aqui</a>.</p>
<h2 id="rodando-o-mongo">Rodando o Mongo</h2>
<p>Para executar esta imagem você pode usar a linha abaixo. Não se esqueça de mudar o <code>MONGO_INITDB_ROOT_USERNAME</code> e <code>MONGO_INITDB_ROOT_PASSWORD</code> para o usuário e senha desejado.</p>
<pre><code>docker run --name mongodb -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=balta -e MONGO_INITDB_ROOT_PASSWORD=e296cd9f mongo
</code></pre>
<h3 id="windows">Windows</h3>
<p>Caso esteja no Windows, com <strong>WSL 2</strong> é importante informar o volume onde este container será executado, utilizando a flag <code>-v ~/docker</code> como mostrado abaixo.</p>
<pre><code>docker run -v ~/docker --name mongodb -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=balta -e MONGO_INITDB_ROOT_PASSWORD=e296cd9f mongo
</code></pre>
<p>Para parar a execução você pode pressionar <kbd>CTRL</kbd> + <kbd>C</kbd> no Terminal. Deste momento em diante, seu container vai aparecer na <strong>Dashboard</strong> do Docker de forma visual, onde você poderá parar ou iniciar ela a qualquer momento.</p>
<h2 id="connection-string">Connection String</h2>
<p>Se você utilizou as mesmas configurações deste artigo, sua <strong>Connection String</strong> será igual abaixo. Caso necessário, modifique as informações que alterou na execução dos containers.</p>
<pre><code>mongodb://balta:e296cd9f@localhost:27017/admin
</code></pre>
<h3 id="gui-client">GUI Client</h3>
<p>Caso queira gerenciar seu banco de uma forma visual, você pode utilizar uma das ferramentas gratuitas abaixo:</p>
<ul>
<li><a href="https://www.mongodb.com/try/download/compass">MongoDb Compass</a></li>
</ul>
<h2 id="erros-comuns">Erros comuns</h2>
<h3 id="authentication-failed">Authentication Failed</h3>
<p>Alguns casos podem apresentar erro de autenticação, principalmente em bancos que não sejam o admin.</p>
<p>Para adicionar um novo usuário ao Mongo, execute os passos abaixo substituindo o <code>NOME_DO_BANCO</code> pelo nome do banco que deseja criar o acesso.</p>
<p>Estes passos precisam ser executados em um terminal com acesso ao Docker, assim como nos passos anteriores.</p>
<pre><code>docker exec mongo

db.createUser(
{
   user: &quot;balta&quot;,
   pwd: &quot;baltaio&quot;,
   roles: [
     { role: &quot;readWrite&quot;, db: &quot;NOME_DO_BANCO&quot; }
   ]
})
</code></pre>


            </article>
        </div>
        <div class="uk-width-1-3@m uk-padding">
            <h3>Populares</h3>
                
<div>
    <h5><a class="uk-text-emphasis" href="/blog/aspnet-5-autenticacao-autorizacao-bearer-jwt">ASP.NET 5 &#x2013; Autentica&#xE7;&#xE3;o e Autoriza&#xE7;&#xE3;o com Bearer e JWT</a></h5>
    <p class="uk-margin-remove">
        
Este artigo atualmente utiliza a vers&#xE3;o 5.0.0-rc.1 do ASP.NET/.NET, o que significa que ainda n&#xE3;o...
    </p>
    <hr />
</div>
                
<div>
    <h5><a class="uk-text-emphasis" href="/blog/clean-code">Clean Code - Guia e Exemplos</a></h5>
    <p class="uk-margin-remove">
        
Saiba como manter seu c&#xF3;digo limpo (Clean Code) seguindo algumas pr&#xE1;ticas sugeridas pelo Robert C...
    </p>
    <hr />
</div>
                
<div>
    <h5><a class="uk-text-emphasis" href="/blog/git-github-primeiros-passos">Git e GitHub - Instala&#xE7;&#xE3;o, Configura&#xE7;&#xE3;o e Primeiros Passos</a></h5>
    <p class="uk-margin-remove">
        
Git &#xE9; um sistema de controle de vers&#xF5;es distribu&#xED;das, enquanto GitHub &#xE9; uma plataforma que tem o ...
    </p>
    <hr />
</div>
                
<div>
    <h5><a class="uk-text-emphasis" href="/blog/visual-studio-code-instalacao-customizacao">Visual Studio Code - Instala&#xE7;&#xE3;o e Customiza&#xE7;&#xE3;o</a></h5>
    <p class="uk-margin-remove">
        
O Visual Studio Code &#xE9; um editor de c&#xF3;digo criado pela Microsoft e que tem uma grande ado&#xE7;&#xE3;o pela...
    </p>
    <hr />
</div>
                
<div>
    <h5><a class="uk-text-emphasis" href="/blog/angular-rotas-guardas-navegacao">Angular: Rotas, Guardas e Navega&#xE7;&#xE3;o</a></h5>
    <p class="uk-margin-remove">
        
O Angular nos fornece um esquema de rotas e navega&#xE7;&#xE3;o completo, simples e f&#xE1;cil de utilizar.
    </p>
    <hr />
</div>
        </div>
    </div>



    <small class="uk-text-emphasis"><strong>Compartilhe este artigo</strong></small> <br /><br />
    <a title="Compartilhar no LinkedIn" class="uk-icon-button" uk-icon="linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=https://balta.io/artigos/mongodb-docker"></a>
<a title="Compartilhar no Facebook" class="uk-icon-button" uk-icon="facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://balta.io/artigos/mongodb-docker"></a>
<a title="Compartilhar no Twitter" class="uk-icon-button" uk-icon="twitter" target="_blank" href="https://twitter.com/home?status=https://balta.io/artigos/mongodb-docker"></a>
    <br /><br />

    <div class="uk-width-1-2@m">
        <h4>Conheça o autor</h4>
        
<div class="uk-grid-small uk-flex-middle" uk-grid>
    <div class="uk-width-auto">
        <a class="no-link" href="/contribuidores/andre-baltieri">
            <img class="uk-border-circle" width="40" height="40" src="https://baltaio.blob.core.windows.net/static/images/authors/andrebaltieri.jpg" alt="Andr&#xE9; Baltieri">
        </a>
    </div>
    <div class="uk-width-expand">
        <a class="no-link" href="/contribuidores/andre-baltieri">
            <p class="uk-margin-remove-bottom uk-text-emphasis uk-text-bold">Andr&#xE9; Baltieri</p>
            <p class="uk-text-meta uk-margin-remove-top">Microsoft MVP</p>
        </a>
    </div>
</div>
        <p><small>Me dedico ao desenvolvimento de software desde 2003, sendo minha maior especialidade o Desenvolvimento Web. Durante esta jornada pude trabalhar presencialmente aqui no Brasil e Estados Unidos, atender remotamente times da ?ndia, Inglaterra e Holanda, receber <a class="uk-text-emphasis" href="https://mvp.microsoft.com/pt-br/PublicProfile/5000060" target="_blank"><strong>8x Microsoft MVP</strong></a> e realizar diversas consultorias em empresas e projetos de todos os tamanhos.</small></p>
    </div>
</div>

<br>
<br>
<br>
<br>
<div class="background-dark-light">
    <div class="uk-container">
        <div class="uk-margin-large-top uk-margin-large-bottom uk-text-center uk-grid-match" uk-grid>
            <div class="uk-width-1-4@l uk-width-1-2@s uk-text-center">
                <h2 class="uk-padding-remove uk-margin-remove">1.889</h2>
                <p class="uk-padding-remove uk-margin-remove">Aulas disponíveis</p>
            </div>
            <div class="uk-width-1-4@l uk-width-1-2@s uk-text-center">
                <h2 class="uk-padding-remove uk-margin-remove">274</h2>
                <p class="uk-padding-remove uk-margin-remove">horas de conteúdo</p>
            </div>
            <div class="uk-width-1-4@l uk-width-1-2@s uk-text-center">
                <h2 class="uk-padding-remove uk-margin-remove">31.124</h2>
                <p class="uk-padding-remove uk-margin-remove">Alunos matriculados</p>
            </div>
            <div class="uk-width-1-4@l uk-width-1-2@s uk-text-center">
                <h2 class="uk-padding-remove uk-margin-remove">23.174</h2>
                <p class="uk-padding-remove uk-margin-remove">Certificados emitidos</p>
            </div>
        </div>
    </div>
</div>

<br>
<br>
<br>
<br>

<div class="uk-container">
    <div class="uk-padding-large-top uk-flex uk-flex-column@s uk-flex-row@m uk-flex-between uk-flex-middle uk-background-primary uk-border-rounded uk-padding"
        style="border-bottom: 4px solid #FF508F;">
        <div>
            <h3 class="uk-padding-remove uk-margin-remove">Comece de graça agora mesmo!</h3>
            <p class="uk-padding-remove uk-margin-remove">
                <small>Temos mais de 16 cursos totalmente de graça e todos com certificado de
                    conclusão.</small>
            </p>
        </div>
        <div>
            <a href="https://balta.io/comecar" class="uk-button uk-button-secondary">Começar</a>
        </div>
    </div>
</div>

<br>
<br>

<div class="uk-container">
    <div class="uk-margin-large-top">
        <div class="uk-text-center">
            <p class="uk-padding-remove uk-margin-remove uk-text-bolder">Prefere algo mais Premium?</p>
            <h2 class="uk-padding-remove uk-margin-remove">Conheça nossos planos</h2>
        </div>
    </div>

    <br>
    <br>

    <div class="uk-flex uk-flex-center uk-flex-middle" uk-grid>

        <div class="uk-width-1-3@l uk-width-1-2@s">
            <div class="uk-card uk-card-default uk-card-body uk-flex uk-flex-column uk-flex-middle">
                <h4 class="uk-padding-remove uk-margin-remove">Premium mensal</h4>
                <p class="uk-padding-remove uk-margin-remove uk-text-center">Cobrado mensalmente via<br>cartão de crédito</p>
                <br>
                <div class="uk-flex uk-flex-row uk-margin-top uk-margin-bottom">
                    <h2>ESGOTADO</h2>
                </div>

                <div class="uk-width-1-1">
                    <ul class="uk-list">
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Acesso à todo conteúdo</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Emissão de Certificado</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>56 cursos disponíveis</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>4 carreiras disponíveis</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>161 temas de tecnologia</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Conteúdo novo todo mês</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Cobrado mensalmente</li>
                    </ul>
                </div>
                <br>
                <a href="https://go.balta.io/lista-espera-mensal" class="uk-button uk-button-secondary uk-width-1-1">Me Avise</a>
                <p>
                    <a href="">Política de privacidade</a>
                </p>
            </div>
        </div>

        <div class="uk-width-1-3@l uk-width-1-2@s">
            <div class="uk-card uk-card-default uk-card-body uk-flex uk-flex-column uk-flex-middle border-fullstack">
                <h4 class="uk-padding-remove uk-margin-remove">Premium semestral</h4>
                <p class="uk-padding-remove uk-margin-remove uk-text-center">Compra única, parcelada em até<br>12x no cartão de crédito</p>
                <br>
                <div class="uk-flex uk-flex-row uk-margin-top uk-margin-bottom">
                    <div class="uk-flex uk-flex-column uk-flex-top uk-text-right uk-margin-small-right">
                        <strong>12x</strong>
                        <span>R$</span>
                    </div>
                    <div>
                        <h3 class="uk-heading-large">33</h3>
                    </div>
                    <div class="uk-flex uk-flex-column uk-flex-start uk-flex-left uk-margin-small-left">
                        <h2 class="uk-padding-remove uk-margin-remove">,33</h2>
                        <span>=R$ 399,90</span>
                    </div>
                </div>

                <div class="uk-width-1-1">
                    <ul class="uk-list">
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>6 meses de acesso</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Acesso à todo conteúdo</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Emissão de Certificado</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Tira Dúvidas Online</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>56 cursos disponíveis</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>4 carreiras disponíveis</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>161 temas de tecnologia</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Conteúdo novo todo mês</li>
                        <li>
                            <span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span><a href="">Encontros Premium</a>
                        </li>
                    </ul>
                </div>
                <br>
                <a class="uk-button uk-button-primary uk-width-1-1" href="/checkout/semestral">Começar agora</a>
                <p>
                    <a href="">Política de privacidade</a>
                </p>
            </div>
        </div>

        <div class="uk-width-1-3@l uk-width-1-2@s">
            <div class="uk-card uk-card-default uk-card-body uk-flex uk-flex-column uk-flex-middle">
                <h4 class="uk-padding-remove uk-margin-remove">Premium anual</h4>
                <p class="uk-padding-remove uk-margin-remove uk-text-center">Compra única, parcelada em até<br>12x no cartão de crédito</p>
                <br>
                <div class="uk-flex uk-flex-row uk-margin-top uk-margin-bottom">
                    <div class="uk-flex uk-flex-column uk-flex-top uk-text-right uk-margin-small-right">
                        <strong>12x</strong>
                        <span>R$</span>
                    </div>
                    <div>
                        <h3 class="uk-heading-large">64</h3>
                    </div>
                    <div class="uk-flex uk-flex-column uk-flex-start uk-flex-left uk-margin-small-left">
                        <h2 class="uk-padding-remove uk-margin-remove">,99</h2>
                        <span>=R$ 779,90</span>
                    </div>
                </div>

                <div class="uk-width-1-1">
                    <ul class="uk-list">
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>1 ano de acesso</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Acesso à todo conteúdo</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Emissão de Certificado</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Tira Dúvidas Online</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>56 cursos disponíveis</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>4 carreiras disponíveis</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>161 temas de tecnologia</li>
                        <li><span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span>Conteúdo novo todo mês</li>
                        <li>
                            <span class="uk-text-secondary uk-text-bolder uk-margin-large-left@s uk-margin-right" uk-icon="icon: check; ratio: 1"></span><a href="">Encontros Premium</a>
                        </li>
                    </ul>
                </div>
                <br>
                <a class="uk-button uk-button-primary uk-width-1-1" href="/checkout/anual">Começar agora</a>
                <p>
                    <a href="">Política de privacidade</a>
                </p>
            </div>
        </div>


    </div>
</div>

<br>
<br>

<div class="uk-container">
    <div class="uk-margin-large-top">
        <div class="uk-text-center">
            <p class="uk-padding-remove uk-margin-remove uk-text-bolder">Precisa de ajuda?</p>
            <h2 class="uk-padding-remove uk-margin-remove">Dúvidas frequentes</h2>
        </div>
    </div>

    <br>
    <br>

    <div class="uk-flex uk-flex-center">
        <ul uk-accordion class="uk-width-1-2@m">
            <li>
                <a class="uk-accordion-title" href="#">Posso começar de graça?</a>
                <div class="uk-accordion-content">
                    <p>
                        Sim! Basta <a href="https://balta.io/comecar" class="uk-text-emphasis" target="_blank"><u>criar
                                sua conta gratuita</u></a> no balta.io e começar seus estudos. Nós contamos com <a class="uk-text-emphasis" href=""><u>diversos cursos TOTALMENTE
                                gratuitos</u></a> e com certificado de conclusão.
                    </p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">Vou ter que pagar algo?</a>
                <div class="uk-accordion-content">
                    <p>
                        Nós temos cursos gratuitos e pagos, porém você não precisa informar nenhum dado de pagamento
                        para começar seus estudos gratuitamente conosco. Os cursos gratuitos são completos e com
                        certificado de conclusão, você não paga nada por eles. <br />
                        <br />
                        Porém, caso queira algo mais <a class="uk-text-emphasis" href=""><u>Premium</u></a>,
                        você terá acesso à diversos benefícios que vão te ajudar ainda mais em sua carreira.
                    </p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">Por onde devo começar?</a>
                <div class="uk-accordion-content">
                    <p>
                        Siga SEMPRE as nossas <a href=""><u>Carreiras</u></a>, elas vão te orientar em todos
                        os sentidos. Os cursos já estão organizados em categorias e carreiras para facilitar seu
                        aprendizado. <br />
                        Nossa sugestão para aprendizado é começar pelo Backend e seguindo para Frontend e Mobile. <br />
                    <ul>
                        <li>Backend</li>
                        <li>Frontend</li>
                        <li>Mobile</li>
                    </ul>
                    </p>
                </div>
            </li>


            <li>
                <a class="uk-accordion-title" href="#">Os cursos ensinam tudo que preciso?</a>
                <div class="uk-accordion-content">
                    <p>
                        Nenhum curso no mundo vai te ensinar tudo, desculpa ser sincero! Os cursos são uma base, eles
                        fornecem por volta de 30% do que você precisa aprender, o resto é com você, com dedicação e
                        MUITA prática.
                    </p>
                </div>
            </li>
            <li>
                <a class="uk-accordion-title" href="#">O que eu devo estudar?</a>
                <div class="uk-accordion-content">
                    <p>
                        Java ou .NET? Angular ou React? Xamarin ou Flutter? A resposta é simples e direta: "Você já sabe
                        o básico?" <br />
                        <br />
                        Se você ainda não sabe BEM o básico, ou seja, os fundamentos, OOP, SOLID, Clean Code, está
                        perdendo tempo estudando Frameworks ou até coisas mais avançadas como Docker. Foque nos seus
                        objetivos primeiro.
                        <br />
                        Agora se você está indeciso sobre qual Framework estudar, a boa notícia é que o mercado neste
                        momento está bem aquecido e você tem várias oportunidade. Desta forma o que levaríamos em conta
                        para tomar esta decisão seria:
                        <br />
                    <ul>
                        <li>Já sei o básico</li>
                        <li>O Framework/Tecnologia tem mercado onde eu estou (região)</li>
                        <li>O Framework/Tecnologia é utilizado em uma empresa onde quero atual</li>
                        <li>O Framework/Tecnologia resolve meu problema</li>
                        <li>Eu gosto de utilizar o Framework/Tecnologia</li>
                    </ul>
                    </p>
                </div>
            </li>

            <li>
                <a class="uk-accordion-title" href="#">Estou pronto para estudar no balta.io?</a>
                <div class="uk-accordion-content">
                    <p>
                        Com certeza! O primeiro passo é começar e você pode fazer isto agora mesmo! <br />
                        <br />
                        <a href="https://balta.io/comecar" class="uk-button uk-button-secondary">Começar de
                            graça</a>
                    </p>
                </div>
            </li>
        </ul>
    </div>
    <p class="uk-text-center uk-margin-top">
        <a class="uk-text-emphasis" href="/guia"><strong><u>Ainda tem dúvidas?</u></strong></a>
    </p>
</div>

<br>
<br>
<br>
<br>

<div class="background-dark-light">
    <div class="uk-container">
        <div id="modal-newsletter" class="uk-modal-full" uk-modal>
            <div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle uk-height-viewport">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                <div role="main" id="newsletter-site-2020-3abf120030efac79c090"></div>
            </div>
        </div>

        <div class="uk-padding-large-top uk-flex uk-flex-column@s uk-flex-row@m uk-flex-between uk-flex-middle uk-border-rounded uk-padding">
            <div>
                <h3 class="uk-padding-remove uk-margin-remove">Assine nosso Newsletter</h3>
                <p class="uk-padding-remove uk-margin-remove">
                    <small>Receba em primeira mão todas as nossas novidades.</small>
                </p>
            </div>
            <div>
                <a href="#modal-newsletter" uk-toggle class="uk-button uk-button-primary">Cadastrar</a>
            </div>
        </div>
    </div>
</div>

<br>
<br>


</main>

<div class="uk-container">
    <div class="uk-margin-large-top" uk-grid>
        <div class="uk-width-1-4@l uk-width-1-2@s">
            <img src="https://baltaio.blob.core.windows.net/static/images/dark/balta-logo.svg"
                 class="uk-width-1-2" alt="balta.io" />
        </div>
        <div class="uk-width-expand">
            <div uk-grid>
                <div class="uk-width-1-4@l uk-width-1-2@s">
                    <h4 class="uk-padding-remove uk-margin-remove">Sobre</h4>
                    <ul class="uk-list">
                        <li><a href="/como-funciona">Como funciona?</a></li>
                        <li><a href="">Seja Premium</a></li>
                        <li><a href="/agenda">Agenda</a></li>
                        <li><a href="/blog">Blog</a></li>
                        <li><a href="/cursos">Todos os cursos</a></li>
                    </ul>
                </div>

                <div class="uk-width-1-4@l uk-width-1-2@s">
                    <h4 class="uk-padding-remove uk-margin-remove">Cursos</h4>
                    <ul class="uk-list">
                        <li><a href="/categorias/frontend">Frontend</a></li>
                        <li><a href="/categorias/backend">Backend</a></li>
                        <li><a href="/categorias/mobile">Mobile</a></li>
                        <li><a href="/categorias/fullstack">Fullstack</a></li>
                    </ul>
                </div>

                <div class="uk-width-1-4@l uk-width-1-2@s">
                    <h4 class="uk-padding-remove uk-margin-remove">Suporte</h4>
                    <ul class="uk-list">
                        <li><a href="/politicas/uso">Termos de uso</a></li>
                        <li><a href="/politicas/privacidade">Privacidade</a></li>
                        <li><a href="/politicas/cancelamento">Cancelamento</a></li>
                        <li><a href="/ajuda">Central de ajuda</a></li>
                    </ul>
                </div>

                <div class="uk-width-1-4@l uk-width-1-2@s">
                    <h4 class="uk-padding-remove uk-margin-remove">Redes Sociais</h4>
                    <ul class="uk-list">
                        <li><a href="https://t.me/baltaio" target="_blank">Telegram</a></li>
                        <li><a href="https://www.facebook.com/balta.io" target="_blank">Facebook</a></li>
                        <li><a href="https://instagram.com/balta.io" target="_blank">Instagram</a></li>
                        <li><a href="https://www.youtube.com/c/baltaio" target="_blank">YouTube</a></li>
                        <li><a href="https://www.twitch.tv/balta_io" target="_blank">Twitch</a></li>
                        <li><a href="https://www.linkedin.com/company/balta-io/" target="_blank">LinkedIn</a></li>
                        <li><a href="https://discord.gg/jHpDFvX3ak" target="_blank">Discord</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<br>

<script src="https://baltaio.blob.core.windows.net/static/js/uikit.min.js?v1"></script>
<script src="https://baltaio.blob.core.windows.net/static/js/uikit-icons.min.js?v1"></script>

<script src="https://baltaio.blob.core.windows.net/static/js/jquery-3.5.1.min.js?v1"></script>
<script src="https://baltaio.blob.core.windows.net/static/js/jquery.validate.min.js?v1"></script>
<script src="https://baltaio.blob.core.windows.net/static/js/jquery.validate.unobtrusive.min.js?v1"></script>
<script src="https://baltaio.blob.core.windows.net/static/js/additional-methods.min.js?v1"></script>

<script type="text/javascript" async
        src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/89adbcfe-3c71-467f-9fdb-a721793afc26-loader.js"></script>
<script type="text/javascript"
        src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
<script type="text/javascript">new RDStationForms('newsletter-site-2020-3abf120030efac79c090', 'UA-48664517-12').createForm();</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-48664517-12"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-48664517-12');
</script>

<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
        document,
        'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '140466216814592');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1" style="display: none"
         src="https://www.facebook.com/tr?id=140466216814592&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->
<!-- OneSignal -->
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(function () {
        OneSignal.init({
            appId: "0241ee6c-703e-4fb8-aa73-91a27039ea52",
        });
    });
</script>


    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.1.2/build/styles/dracula.min.css">
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.1.2/build/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</body>

</html>